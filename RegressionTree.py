#### Algorithm ####
'''
DATA
Training Data Looks Like:
[
    {
        "porosity": 0.142, 
        "gamma": 1.43,
        "sonic": 2943.11328,
        "density": -0.32432323, 
        "bpd": 166.232314, # Label
    },
    ...
]

CORE IDEA
If we Split Data by specific Feature, how well does it Split our Labels ?
- Answered with Gini Impurity OR Mean Squared Error (MSE)
Once have Split Points recurssed down entire Tree, based on Training Data...
... Can then Traverse to Leaf with New Example and Average of Leaf Node Examples is Label of New Example

TRAIN
1. Order Examples by current Column/Feature
2. Average between each Adjacent Example's Data for that Column/Feature
3. Each Average is Split Point to Evaluate
4. Split Data accordingly (i.e. Left Node and Right Node)
5. Evaluate Split (via MSE in this case for Continuous Labels)
6. Repeat for all Adjacent Pairs of given Column/Feature, and for each Column/Feature
-- Track Minimum MSE Split Feature for that Level in Tree
7. Split Data on that Min MSE Split, repeat for all again on each of below Nodes
8. Recurse repeatedly till some Stopping Point (e.g. some Min # Examples in Node, our case = 1 Example)

PREDICT
1. Traverse Exaple down to Leaf Node (abiding by learned Splits for that Example)
2. Average Labels of Examples in final Leaf Node (in case of Regression Tree)


NOTE
Here, we are not handling Missing Data (could track N Best Split Points and ignore Evaluated Splits of 0)
'''



class TreeNode:
    def __init__(self, examples):
        # Examples: Feature Key -> Values, and "bpd" -> Label
        self.examples = examples
        self.left = None
        self.right = None
        self.split_point = None
        self.split_feature = None

    def split(self):
        if len(self.examples) == 0: return; # 1 Example = No reason to Split

        min_feature = None;
        min_split_point = float("inf"); # Some large initial value to promote future change
        min_mse = float("inf");

        # TODO: Using Index could be more Memory efficient -- I..e Split at (min) Index
        #       But may require another Sort to properly split List
        min_less_than_examples = [];
        min_greater_equal_to_examples = []
        
        for featureName in self.examples[0].keys():
            if featureName == "bpd": continue; # Don't Split on the Label
            orderedExamples = TreeNode.orderExamples( self.examples, featureName );

            # Evaluate all Feature Values as potential Split Points
            for exampleIndex in range( 1, len(self.examples) ):
                exampleOne = orderedExamples[ exampleIndex - 1];
                exampleTwo = orderedExamples[ exampleIndex ];

                # Average of Two Adjacent Examples give potential Split Point Value
                split_point_candidate = (exampleOne[featureName] + exampleTwo[featureName]) / 2;

                [less_than_examples, greater_equal_to_examples] = RegressionTree.splitDataBySplitPoint( orderedExamples, featureName, split_point_candidate );
                
                mse = RegressionTree.calculate_mse( less_than_examples, greater_equal_to_examples, "bpd" );

                if mse < min_mse:
                    min_mse = mse
                    min_feature = featureName;
                    min_split_point = split_point_candidate;

                    # NOTE: As mentioned above, can save Memory by using Index instead
                    min_less_than_examples = less_than_examples;
                    min_greater_equal_to_examples = greater_equal_to_examples;

        self.split_feature = min_feature;
        self.split_point = min_split_point;

        if len( min_less_than_examples ) > 0:
            self.left = TreeNode( min_less_than_examples );
            self.left.split()
        else:
            self.left = None
            
        if len( min_greater_equal_to_examples ) > 0:
            self.right = TreeNode( min_greater_equal_to_examples );
            self.right.split()
        else:
            self.right = None

        
    ################# CLASS METHODS #################
    def orderExamples( examples, feature ):
        return sorted( examples, key=lambda d: d[feature] );

    
    def traverseToLeaf( node, example ):
        if node.left == None and node.right == None: return node;
        
        feature = node.split_feature

        if feature == None: return;

        # NOTE: Uses Recursion, more Memory intense for Recursion Stack
        #       BUT, more intuitive/cleaner (in my opinion)
        if example[feature] < node.split_point:
            if node.left != None: 
                return TreeNode.traverseToLeaf( node.left, example );
        else:
            if node.right != None: 
                return TreeNode.traverseToLeaf( node.right, example );

        return node;


class RegressionTree:
    def __init__(self, examples):
        self.root = TreeNode(examples)
        self.train()

    def train(self):
        self.root.split()

    def predict(self, example):
        leaf_node = TreeNode.traverseToLeaf( self.root, example );
        leaf_examples = leaf_node.examples;
        return RegressionTree.calculateNodeAverage( leaf_examples, "bpd" );


            
    ################# CLASS METHODS #################
    def splitDataBySplitPoint( examples, feature, split_point ):
        # NOTE: Could do via List Comprehension -- E.g. [ example["bpd"] for example in self.examples if example[feature] <= split_point]
        less_than_examples = [];
        greater_equal_to_examples = [];
    
        for example in examples:
            if example[feature] < split_point:
                less_than_examples.append( example );
            else:
                greater_equal_to_examples.append( example );
    
        return [less_than_examples, greater_equal_to_examples];

    
    def calculate_mse( left_node_examples, right_node_examples, feature ):
        # NOTE: This calculates TOTAL MSE...
        #     ... If want evaluate Node-Level MSE,
        #     ... Would need to have this calculate Node specific MSE, 
        #     ... Then combine in some way at caller-level for final/combined MSE
        #     ... Could by having two methods, one, calculate_node_mse and then get_split_point_mse which calls that
        mse = 0;

        left_node_average = RegressionTree.calculateNodeAverage( left_node_examples, feature );
        right_node_average = RegressionTree.calculateNodeAverage( right_node_examples, feature );

        for example in left_node_examples:
            difference = example[feature] - left_node_average;
            mse += difference * difference;

        for example in right_node_examples:
            difference = example[feature] - right_node_average;
            mse += difference * difference;

        total_examples = len(left_node_examples) + len(right_node_examples);
        return mse / total_examples;

    
    def calculateNodeAverage( examples, feature ):
        if examples == None: return;
            
        sum = 0;

        for example in examples:
            sum += example[feature];

        return sum / len(examples);
